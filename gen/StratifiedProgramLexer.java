// Generated from C:/Users/Baptiste/Desktop/lyon1/statification-module-for-datalog-programs\StratifiedProgram.g4 by ANTLR 4.8
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class StratifiedProgramLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		EDB=1, IDB=2, OPAR=3, CPAR=4, COM=5, DOT=6, NOT=7, QUOTE=8, AFFECTATION=9, 
		UNDERLINE=10, NAME=11, VARIABLE=12, INT=13, STRING=14, COMMENT=15, SPACE=16;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"EDB", "IDB", "OPAR", "CPAR", "COM", "DOT", "NOT", "QUOTE", "AFFECTATION", 
			"UNDERLINE", "NAME", "VARIABLE", "INT", "STRING", "COMMENT", "SPACE"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'%edb'", "'%idb'", "'('", "')'", "','", "'.'", "'not'", "'\"'", 
			"':-'", "'_'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "EDB", "IDB", "OPAR", "CPAR", "COM", "DOT", "NOT", "QUOTE", "AFFECTATION", 
			"UNDERLINE", "NAME", "VARIABLE", "INT", "STRING", "COMMENT", "SPACE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public StratifiedProgramLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "StratifiedProgram.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\22s\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\3\2\3\2\3"+
		"\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b"+
		"\3\b\3\b\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3\f\3\f\7\fC\n\f\f\f\16\fF\13\f"+
		"\3\r\6\rI\n\r\r\r\16\rJ\3\r\7\rN\n\r\f\r\16\rQ\13\r\3\16\6\16T\n\16\r"+
		"\16\16\16U\3\17\3\17\3\17\3\17\7\17\\\n\17\f\17\16\17_\13\17\3\17\3\17"+
		"\3\20\3\20\3\20\5\20f\n\20\3\20\7\20i\n\20\f\20\16\20l\13\20\3\20\3\20"+
		"\3\21\3\21\3\21\3\21\2\2\22\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25"+
		"\f\27\r\31\16\33\17\35\20\37\21!\22\3\2\t\3\2C\\\3\2c|\4\2C\\c|\3\2\62"+
		";\5\2\f\f\17\17$$\4\2\f\f\17\17\5\2\13\f\17\17\"\"\2z\2\3\3\2\2\2\2\5"+
		"\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2"+
		"\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33"+
		"\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\3#\3\2\2\2\5(\3\2\2\2\7"+
		"-\3\2\2\2\t/\3\2\2\2\13\61\3\2\2\2\r\63\3\2\2\2\17\65\3\2\2\2\219\3\2"+
		"\2\2\23;\3\2\2\2\25>\3\2\2\2\27@\3\2\2\2\31H\3\2\2\2\33S\3\2\2\2\35W\3"+
		"\2\2\2\37e\3\2\2\2!o\3\2\2\2#$\7\'\2\2$%\7g\2\2%&\7f\2\2&\'\7d\2\2\'\4"+
		"\3\2\2\2()\7\'\2\2)*\7k\2\2*+\7f\2\2+,\7d\2\2,\6\3\2\2\2-.\7*\2\2.\b\3"+
		"\2\2\2/\60\7+\2\2\60\n\3\2\2\2\61\62\7.\2\2\62\f\3\2\2\2\63\64\7\60\2"+
		"\2\64\16\3\2\2\2\65\66\7p\2\2\66\67\7q\2\2\678\7v\2\28\20\3\2\2\29:\7"+
		"$\2\2:\22\3\2\2\2;<\7<\2\2<=\7/\2\2=\24\3\2\2\2>?\7a\2\2?\26\3\2\2\2@"+
		"D\t\2\2\2AC\t\3\2\2BA\3\2\2\2CF\3\2\2\2DB\3\2\2\2DE\3\2\2\2E\30\3\2\2"+
		"\2FD\3\2\2\2GI\t\3\2\2HG\3\2\2\2IJ\3\2\2\2JH\3\2\2\2JK\3\2\2\2KO\3\2\2"+
		"\2LN\t\4\2\2ML\3\2\2\2NQ\3\2\2\2OM\3\2\2\2OP\3\2\2\2P\32\3\2\2\2QO\3\2"+
		"\2\2RT\t\5\2\2SR\3\2\2\2TU\3\2\2\2US\3\2\2\2UV\3\2\2\2V\34\3\2\2\2W]\7"+
		"$\2\2X\\\n\6\2\2YZ\7$\2\2Z\\\7$\2\2[X\3\2\2\2[Y\3\2\2\2\\_\3\2\2\2][\3"+
		"\2\2\2]^\3\2\2\2^`\3\2\2\2_]\3\2\2\2`a\7$\2\2a\36\3\2\2\2bf\7%\2\2cd\7"+
		"\61\2\2df\7\61\2\2eb\3\2\2\2ec\3\2\2\2fj\3\2\2\2gi\n\7\2\2hg\3\2\2\2i"+
		"l\3\2\2\2jh\3\2\2\2jk\3\2\2\2km\3\2\2\2lj\3\2\2\2mn\b\20\2\2n \3\2\2\2"+
		"op\t\b\2\2pq\3\2\2\2qr\b\21\2\2r\"\3\2\2\2\13\2DJOU[]ej\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}