// Generated from C:/Users/Baptiste/Desktop/lyon1/statification-module-for-datalog-programs\StratifiedProgram.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link StratifiedProgramParser}.
 */
public interface StratifiedProgramListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code progRule}
	 * labeled alternative in {@link StratifiedProgramParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProgRule(StratifiedProgramParser.ProgRuleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code progRule}
	 * labeled alternative in {@link StratifiedProgramParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProgRule(StratifiedProgramParser.ProgRuleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code edbLine}
	 * labeled alternative in {@link StratifiedProgramParser#line}.
	 * @param ctx the parse tree
	 */
	void enterEdbLine(StratifiedProgramParser.EdbLineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code edbLine}
	 * labeled alternative in {@link StratifiedProgramParser#line}.
	 * @param ctx the parse tree
	 */
	void exitEdbLine(StratifiedProgramParser.EdbLineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code edbRuleLine}
	 * labeled alternative in {@link StratifiedProgramParser#line}.
	 * @param ctx the parse tree
	 */
	void enterEdbRuleLine(StratifiedProgramParser.EdbRuleLineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code edbRuleLine}
	 * labeled alternative in {@link StratifiedProgramParser#line}.
	 * @param ctx the parse tree
	 */
	void exitEdbRuleLine(StratifiedProgramParser.EdbRuleLineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code idbLine}
	 * labeled alternative in {@link StratifiedProgramParser#line}.
	 * @param ctx the parse tree
	 */
	void enterIdbLine(StratifiedProgramParser.IdbLineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code idbLine}
	 * labeled alternative in {@link StratifiedProgramParser#line}.
	 * @param ctx the parse tree
	 */
	void exitIdbLine(StratifiedProgramParser.IdbLineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code idbruleLine}
	 * labeled alternative in {@link StratifiedProgramParser#line}.
	 * @param ctx the parse tree
	 */
	void enterIdbruleLine(StratifiedProgramParser.IdbruleLineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code idbruleLine}
	 * labeled alternative in {@link StratifiedProgramParser#line}.
	 * @param ctx the parse tree
	 */
	void exitIdbruleLine(StratifiedProgramParser.IdbruleLineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code argList}
	 * labeled alternative in {@link StratifiedProgramParser#args_l}.
	 * @param ctx the parse tree
	 */
	void enterArgList(StratifiedProgramParser.ArgListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code argList}
	 * labeled alternative in {@link StratifiedProgramParser#args_l}.
	 * @param ctx the parse tree
	 */
	void exitArgList(StratifiedProgramParser.ArgListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code argAlone}
	 * labeled alternative in {@link StratifiedProgramParser#args_l}.
	 * @param ctx the parse tree
	 */
	void enterArgAlone(StratifiedProgramParser.ArgAloneContext ctx);
	/**
	 * Exit a parse tree produced by the {@code argAlone}
	 * labeled alternative in {@link StratifiedProgramParser#args_l}.
	 * @param ctx the parse tree
	 */
	void exitArgAlone(StratifiedProgramParser.ArgAloneContext ctx);
	/**
	 * Enter a parse tree produced by the {@code noArg}
	 * labeled alternative in {@link StratifiedProgramParser#args_l}.
	 * @param ctx the parse tree
	 */
	void enterNoArg(StratifiedProgramParser.NoArgContext ctx);
	/**
	 * Exit a parse tree produced by the {@code noArg}
	 * labeled alternative in {@link StratifiedProgramParser#args_l}.
	 * @param ctx the parse tree
	 */
	void exitNoArg(StratifiedProgramParser.NoArgContext ctx);
	/**
	 * Enter a parse tree produced by the {@code argVar}
	 * labeled alternative in {@link StratifiedProgramParser#args}.
	 * @param ctx the parse tree
	 */
	void enterArgVar(StratifiedProgramParser.ArgVarContext ctx);
	/**
	 * Exit a parse tree produced by the {@code argVar}
	 * labeled alternative in {@link StratifiedProgramParser#args}.
	 * @param ctx the parse tree
	 */
	void exitArgVar(StratifiedProgramParser.ArgVarContext ctx);
	/**
	 * Enter a parse tree produced by the {@code argInt}
	 * labeled alternative in {@link StratifiedProgramParser#args}.
	 * @param ctx the parse tree
	 */
	void enterArgInt(StratifiedProgramParser.ArgIntContext ctx);
	/**
	 * Exit a parse tree produced by the {@code argInt}
	 * labeled alternative in {@link StratifiedProgramParser#args}.
	 * @param ctx the parse tree
	 */
	void exitArgInt(StratifiedProgramParser.ArgIntContext ctx);
	/**
	 * Enter a parse tree produced by the {@code argString}
	 * labeled alternative in {@link StratifiedProgramParser#args}.
	 * @param ctx the parse tree
	 */
	void enterArgString(StratifiedProgramParser.ArgStringContext ctx);
	/**
	 * Exit a parse tree produced by the {@code argString}
	 * labeled alternative in {@link StratifiedProgramParser#args}.
	 * @param ctx the parse tree
	 */
	void exitArgString(StratifiedProgramParser.ArgStringContext ctx);
	/**
	 * Enter a parse tree produced by the {@code argUnderLine}
	 * labeled alternative in {@link StratifiedProgramParser#args}.
	 * @param ctx the parse tree
	 */
	void enterArgUnderLine(StratifiedProgramParser.ArgUnderLineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code argUnderLine}
	 * labeled alternative in {@link StratifiedProgramParser#args}.
	 * @param ctx the parse tree
	 */
	void exitArgUnderLine(StratifiedProgramParser.ArgUnderLineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code predBuilder}
	 * labeled alternative in {@link StratifiedProgramParser#predicat}.
	 * @param ctx the parse tree
	 */
	void enterPredBuilder(StratifiedProgramParser.PredBuilderContext ctx);
	/**
	 * Exit a parse tree produced by the {@code predBuilder}
	 * labeled alternative in {@link StratifiedProgramParser#predicat}.
	 * @param ctx the parse tree
	 */
	void exitPredBuilder(StratifiedProgramParser.PredBuilderContext ctx);
	/**
	 * Enter a parse tree produced by the {@code predicatWithoutList}
	 * labeled alternative in {@link StratifiedProgramParser#body}.
	 * @param ctx the parse tree
	 */
	void enterPredicatWithoutList(StratifiedProgramParser.PredicatWithoutListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code predicatWithoutList}
	 * labeled alternative in {@link StratifiedProgramParser#body}.
	 * @param ctx the parse tree
	 */
	void exitPredicatWithoutList(StratifiedProgramParser.PredicatWithoutListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notPredicatWithoutList}
	 * labeled alternative in {@link StratifiedProgramParser#body}.
	 * @param ctx the parse tree
	 */
	void enterNotPredicatWithoutList(StratifiedProgramParser.NotPredicatWithoutListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notPredicatWithoutList}
	 * labeled alternative in {@link StratifiedProgramParser#body}.
	 * @param ctx the parse tree
	 */
	void exitNotPredicatWithoutList(StratifiedProgramParser.NotPredicatWithoutListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code predicatWithList}
	 * labeled alternative in {@link StratifiedProgramParser#body}.
	 * @param ctx the parse tree
	 */
	void enterPredicatWithList(StratifiedProgramParser.PredicatWithListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code predicatWithList}
	 * labeled alternative in {@link StratifiedProgramParser#body}.
	 * @param ctx the parse tree
	 */
	void exitPredicatWithList(StratifiedProgramParser.PredicatWithListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notPredicatWithList}
	 * labeled alternative in {@link StratifiedProgramParser#body}.
	 * @param ctx the parse tree
	 */
	void enterNotPredicatWithList(StratifiedProgramParser.NotPredicatWithListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notPredicatWithList}
	 * labeled alternative in {@link StratifiedProgramParser#body}.
	 * @param ctx the parse tree
	 */
	void exitNotPredicatWithList(StratifiedProgramParser.NotPredicatWithListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code predIdbLine}
	 * labeled alternative in {@link StratifiedProgramParser#idbrule}.
	 * @param ctx the parse tree
	 */
	void enterPredIdbLine(StratifiedProgramParser.PredIdbLineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code predIdbLine}
	 * labeled alternative in {@link StratifiedProgramParser#idbrule}.
	 * @param ctx the parse tree
	 */
	void exitPredIdbLine(StratifiedProgramParser.PredIdbLineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code predEdbLine}
	 * labeled alternative in {@link StratifiedProgramParser#edbrule}.
	 * @param ctx the parse tree
	 */
	void enterPredEdbLine(StratifiedProgramParser.PredEdbLineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code predEdbLine}
	 * labeled alternative in {@link StratifiedProgramParser#edbrule}.
	 * @param ctx the parse tree
	 */
	void exitPredEdbLine(StratifiedProgramParser.PredEdbLineContext ctx);
}