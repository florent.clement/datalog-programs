// Generated from C:/Users/Baptiste/Desktop/lyon1/statification-module-for-datalog-programs\StratifiedProgram.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class StratifiedProgramParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		EDB=1, IDB=2, OPAR=3, CPAR=4, COM=5, DOT=6, NOT=7, QUOTE=8, AFFECTATION=9, 
		UNDERLINE=10, NAME=11, VARIABLE=12, INT=13, STRING=14, COMMENT=15, SPACE=16;
	public static final int
		RULE_prog = 0, RULE_line = 1, RULE_args_l = 2, RULE_args = 3, RULE_predicat = 4, 
		RULE_body = 5, RULE_idbrule = 6, RULE_edbrule = 7;
	private static String[] makeRuleNames() {
		return new String[] {
			"prog", "line", "args_l", "args", "predicat", "body", "idbrule", "edbrule"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'%edb'", "'%idb'", "'('", "')'", "','", "'.'", "'not'", "'\"'", 
			"':-'", "'_'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "EDB", "IDB", "OPAR", "CPAR", "COM", "DOT", "NOT", "QUOTE", "AFFECTATION", 
			"UNDERLINE", "NAME", "VARIABLE", "INT", "STRING", "COMMENT", "SPACE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "StratifiedProgram.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public StratifiedProgramParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgContext extends ParserRuleContext {
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
	 
		public ProgContext() { }
		public void copyFrom(ProgContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ProgRuleContext extends ProgContext {
		public TerminalNode EOF() { return getToken(StratifiedProgramParser.EOF, 0); }
		public List<LineContext> line() {
			return getRuleContexts(LineContext.class);
		}
		public LineContext line(int i) {
			return getRuleContext(LineContext.class,i);
		}
		public ProgRuleContext(ProgContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterProgRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitProgRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitProgRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			_localctx = new ProgRuleContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(17); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(16);
				line();
				}
				}
				setState(19); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EDB) | (1L << IDB) | (1L << NAME))) != 0) );
			setState(21);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LineContext extends ParserRuleContext {
		public LineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_line; }
	 
		public LineContext() { }
		public void copyFrom(LineContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class EdbLineContext extends LineContext {
		public TerminalNode EDB() { return getToken(StratifiedProgramParser.EDB, 0); }
		public EdbLineContext(LineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterEdbLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitEdbLine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitEdbLine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EdbRuleLineContext extends LineContext {
		public EdbruleContext edbrule() {
			return getRuleContext(EdbruleContext.class,0);
		}
		public EdbRuleLineContext(LineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterEdbRuleLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitEdbRuleLine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitEdbRuleLine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdbLineContext extends LineContext {
		public TerminalNode IDB() { return getToken(StratifiedProgramParser.IDB, 0); }
		public IdbLineContext(LineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterIdbLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitIdbLine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitIdbLine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdbruleLineContext extends LineContext {
		public IdbruleContext idbrule() {
			return getRuleContext(IdbruleContext.class,0);
		}
		public IdbruleLineContext(LineContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterIdbruleLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitIdbruleLine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitIdbruleLine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LineContext line() throws RecognitionException {
		LineContext _localctx = new LineContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_line);
		try {
			setState(27);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				_localctx = new EdbLineContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(23);
				match(EDB);
				}
				break;
			case 2:
				_localctx = new EdbRuleLineContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(24);
				edbrule();
				}
				break;
			case 3:
				_localctx = new IdbLineContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(25);
				match(IDB);
				}
				break;
			case 4:
				_localctx = new IdbruleLineContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(26);
				idbrule();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Args_lContext extends ParserRuleContext {
		public Args_lContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_args_l; }
	 
		public Args_lContext() { }
		public void copyFrom(Args_lContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ArgListContext extends Args_lContext {
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public TerminalNode COM() { return getToken(StratifiedProgramParser.COM, 0); }
		public Args_lContext args_l() {
			return getRuleContext(Args_lContext.class,0);
		}
		public ArgListContext(Args_lContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterArgList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitArgList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitArgList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NoArgContext extends Args_lContext {
		public NoArgContext(Args_lContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterNoArg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitNoArg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitNoArg(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArgAloneContext extends Args_lContext {
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public ArgAloneContext(Args_lContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterArgAlone(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitArgAlone(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitArgAlone(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Args_lContext args_l() throws RecognitionException {
		Args_lContext _localctx = new Args_lContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_args_l);
		try {
			setState(35);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				_localctx = new ArgListContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(29);
				args();
				setState(30);
				match(COM);
				setState(31);
				args_l();
				}
				break;
			case 2:
				_localctx = new ArgAloneContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(33);
				args();
				}
				break;
			case 3:
				_localctx = new NoArgContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgsContext extends ParserRuleContext {
		public ArgsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_args; }
	 
		public ArgsContext() { }
		public void copyFrom(ArgsContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ArgIntContext extends ArgsContext {
		public TerminalNode INT() { return getToken(StratifiedProgramParser.INT, 0); }
		public ArgIntContext(ArgsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterArgInt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitArgInt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitArgInt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArgStringContext extends ArgsContext {
		public TerminalNode STRING() { return getToken(StratifiedProgramParser.STRING, 0); }
		public ArgStringContext(ArgsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterArgString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitArgString(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitArgString(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArgUnderLineContext extends ArgsContext {
		public TerminalNode UNDERLINE() { return getToken(StratifiedProgramParser.UNDERLINE, 0); }
		public ArgUnderLineContext(ArgsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterArgUnderLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitArgUnderLine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitArgUnderLine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArgVarContext extends ArgsContext {
		public TerminalNode VARIABLE() { return getToken(StratifiedProgramParser.VARIABLE, 0); }
		public ArgVarContext(ArgsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterArgVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitArgVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitArgVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgsContext args() throws RecognitionException {
		ArgsContext _localctx = new ArgsContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_args);
		try {
			setState(41);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VARIABLE:
				_localctx = new ArgVarContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(37);
				match(VARIABLE);
				}
				break;
			case INT:
				_localctx = new ArgIntContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(38);
				match(INT);
				}
				break;
			case STRING:
				_localctx = new ArgStringContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(39);
				match(STRING);
				}
				break;
			case UNDERLINE:
				_localctx = new ArgUnderLineContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(40);
				match(UNDERLINE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredicatContext extends ParserRuleContext {
		public PredicatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicat; }
	 
		public PredicatContext() { }
		public void copyFrom(PredicatContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PredBuilderContext extends PredicatContext {
		public TerminalNode NAME() { return getToken(StratifiedProgramParser.NAME, 0); }
		public TerminalNode OPAR() { return getToken(StratifiedProgramParser.OPAR, 0); }
		public Args_lContext args_l() {
			return getRuleContext(Args_lContext.class,0);
		}
		public TerminalNode CPAR() { return getToken(StratifiedProgramParser.CPAR, 0); }
		public PredBuilderContext(PredicatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterPredBuilder(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitPredBuilder(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitPredBuilder(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PredicatContext predicat() throws RecognitionException {
		PredicatContext _localctx = new PredicatContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_predicat);
		try {
			_localctx = new PredBuilderContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(43);
			match(NAME);
			setState(44);
			match(OPAR);
			setState(45);
			args_l();
			setState(46);
			match(CPAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyContext extends ParserRuleContext {
		public BodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body; }
	 
		public BodyContext() { }
		public void copyFrom(BodyContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NotPredicatWithListContext extends BodyContext {
		public TerminalNode NOT() { return getToken(StratifiedProgramParser.NOT, 0); }
		public PredicatContext predicat() {
			return getRuleContext(PredicatContext.class,0);
		}
		public TerminalNode COM() { return getToken(StratifiedProgramParser.COM, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public NotPredicatWithListContext(BodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterNotPredicatWithList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitNotPredicatWithList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitNotPredicatWithList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NotPredicatWithoutListContext extends BodyContext {
		public TerminalNode NOT() { return getToken(StratifiedProgramParser.NOT, 0); }
		public PredicatContext predicat() {
			return getRuleContext(PredicatContext.class,0);
		}
		public NotPredicatWithoutListContext(BodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterNotPredicatWithoutList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitNotPredicatWithoutList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitNotPredicatWithoutList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PredicatWithoutListContext extends BodyContext {
		public PredicatContext predicat() {
			return getRuleContext(PredicatContext.class,0);
		}
		public PredicatWithoutListContext(BodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterPredicatWithoutList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitPredicatWithoutList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitPredicatWithoutList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PredicatWithListContext extends BodyContext {
		public PredicatContext predicat() {
			return getRuleContext(PredicatContext.class,0);
		}
		public TerminalNode COM() { return getToken(StratifiedProgramParser.COM, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public PredicatWithListContext(BodyContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterPredicatWithList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitPredicatWithList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitPredicatWithList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyContext body() throws RecognitionException {
		BodyContext _localctx = new BodyContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_body);
		try {
			setState(60);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				_localctx = new PredicatWithoutListContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(48);
				predicat();
				}
				break;
			case 2:
				_localctx = new NotPredicatWithoutListContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(49);
				match(NOT);
				setState(50);
				predicat();
				}
				break;
			case 3:
				_localctx = new PredicatWithListContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(51);
				predicat();
				setState(52);
				match(COM);
				setState(53);
				body();
				}
				break;
			case 4:
				_localctx = new NotPredicatWithListContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(55);
				match(NOT);
				setState(56);
				predicat();
				setState(57);
				match(COM);
				setState(58);
				body();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdbruleContext extends ParserRuleContext {
		public IdbruleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_idbrule; }
	 
		public IdbruleContext() { }
		public void copyFrom(IdbruleContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PredIdbLineContext extends IdbruleContext {
		public PredicatContext predicat() {
			return getRuleContext(PredicatContext.class,0);
		}
		public TerminalNode AFFECTATION() { return getToken(StratifiedProgramParser.AFFECTATION, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public TerminalNode DOT() { return getToken(StratifiedProgramParser.DOT, 0); }
		public PredIdbLineContext(IdbruleContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterPredIdbLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitPredIdbLine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitPredIdbLine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdbruleContext idbrule() throws RecognitionException {
		IdbruleContext _localctx = new IdbruleContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_idbrule);
		try {
			_localctx = new PredIdbLineContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(62);
			predicat();
			setState(63);
			match(AFFECTATION);
			setState(64);
			body();
			setState(65);
			match(DOT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EdbruleContext extends ParserRuleContext {
		public EdbruleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_edbrule; }
	 
		public EdbruleContext() { }
		public void copyFrom(EdbruleContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PredEdbLineContext extends EdbruleContext {
		public PredicatContext predicat() {
			return getRuleContext(PredicatContext.class,0);
		}
		public TerminalNode DOT() { return getToken(StratifiedProgramParser.DOT, 0); }
		public PredEdbLineContext(EdbruleContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).enterPredEdbLine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof StratifiedProgramListener ) ((StratifiedProgramListener)listener).exitPredEdbLine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof StratifiedProgramVisitor ) return ((StratifiedProgramVisitor<? extends T>)visitor).visitPredEdbLine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EdbruleContext edbrule() throws RecognitionException {
		EdbruleContext _localctx = new EdbruleContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_edbrule);
		try {
			_localctx = new PredEdbLineContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(67);
			predicat();
			setState(68);
			match(DOT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\22I\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\3\2\6\2\24\n\2\r\2"+
		"\16\2\25\3\2\3\2\3\3\3\3\3\3\3\3\5\3\36\n\3\3\4\3\4\3\4\3\4\3\4\3\4\5"+
		"\4&\n\4\3\5\3\5\3\5\3\5\5\5,\n\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7?\n\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3"+
		"\t\3\t\2\2\n\2\4\6\b\n\f\16\20\2\2\2L\2\23\3\2\2\2\4\35\3\2\2\2\6%\3\2"+
		"\2\2\b+\3\2\2\2\n-\3\2\2\2\f>\3\2\2\2\16@\3\2\2\2\20E\3\2\2\2\22\24\5"+
		"\4\3\2\23\22\3\2\2\2\24\25\3\2\2\2\25\23\3\2\2\2\25\26\3\2\2\2\26\27\3"+
		"\2\2\2\27\30\7\2\2\3\30\3\3\2\2\2\31\36\7\3\2\2\32\36\5\20\t\2\33\36\7"+
		"\4\2\2\34\36\5\16\b\2\35\31\3\2\2\2\35\32\3\2\2\2\35\33\3\2\2\2\35\34"+
		"\3\2\2\2\36\5\3\2\2\2\37 \5\b\5\2 !\7\7\2\2!\"\5\6\4\2\"&\3\2\2\2#&\5"+
		"\b\5\2$&\3\2\2\2%\37\3\2\2\2%#\3\2\2\2%$\3\2\2\2&\7\3\2\2\2\',\7\16\2"+
		"\2(,\7\17\2\2),\7\20\2\2*,\7\f\2\2+\'\3\2\2\2+(\3\2\2\2+)\3\2\2\2+*\3"+
		"\2\2\2,\t\3\2\2\2-.\7\r\2\2./\7\5\2\2/\60\5\6\4\2\60\61\7\6\2\2\61\13"+
		"\3\2\2\2\62?\5\n\6\2\63\64\7\t\2\2\64?\5\n\6\2\65\66\5\n\6\2\66\67\7\7"+
		"\2\2\678\5\f\7\28?\3\2\2\29:\7\t\2\2:;\5\n\6\2;<\7\7\2\2<=\5\f\7\2=?\3"+
		"\2\2\2>\62\3\2\2\2>\63\3\2\2\2>\65\3\2\2\2>9\3\2\2\2?\r\3\2\2\2@A\5\n"+
		"\6\2AB\7\13\2\2BC\5\f\7\2CD\7\b\2\2D\17\3\2\2\2EF\5\n\6\2FG\7\b\2\2G\21"+
		"\3\2\2\2\7\25\35%+>";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}