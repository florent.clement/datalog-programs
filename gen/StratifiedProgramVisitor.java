// Generated from C:/Users/Baptiste/Desktop/lyon1/statification-module-for-datalog-programs\StratifiedProgram.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link StratifiedProgramParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface StratifiedProgramVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code progRule}
	 * labeled alternative in {@link StratifiedProgramParser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgRule(StratifiedProgramParser.ProgRuleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code edbLine}
	 * labeled alternative in {@link StratifiedProgramParser#line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEdbLine(StratifiedProgramParser.EdbLineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code edbRuleLine}
	 * labeled alternative in {@link StratifiedProgramParser#line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEdbRuleLine(StratifiedProgramParser.EdbRuleLineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code idbLine}
	 * labeled alternative in {@link StratifiedProgramParser#line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdbLine(StratifiedProgramParser.IdbLineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code idbruleLine}
	 * labeled alternative in {@link StratifiedProgramParser#line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdbruleLine(StratifiedProgramParser.IdbruleLineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code argList}
	 * labeled alternative in {@link StratifiedProgramParser#args_l}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgList(StratifiedProgramParser.ArgListContext ctx);
	/**
	 * Visit a parse tree produced by the {@code argAlone}
	 * labeled alternative in {@link StratifiedProgramParser#args_l}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgAlone(StratifiedProgramParser.ArgAloneContext ctx);
	/**
	 * Visit a parse tree produced by the {@code noArg}
	 * labeled alternative in {@link StratifiedProgramParser#args_l}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoArg(StratifiedProgramParser.NoArgContext ctx);
	/**
	 * Visit a parse tree produced by the {@code argVar}
	 * labeled alternative in {@link StratifiedProgramParser#args}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgVar(StratifiedProgramParser.ArgVarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code argInt}
	 * labeled alternative in {@link StratifiedProgramParser#args}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgInt(StratifiedProgramParser.ArgIntContext ctx);
	/**
	 * Visit a parse tree produced by the {@code argString}
	 * labeled alternative in {@link StratifiedProgramParser#args}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgString(StratifiedProgramParser.ArgStringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code argUnderLine}
	 * labeled alternative in {@link StratifiedProgramParser#args}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgUnderLine(StratifiedProgramParser.ArgUnderLineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code predBuilder}
	 * labeled alternative in {@link StratifiedProgramParser#predicat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredBuilder(StratifiedProgramParser.PredBuilderContext ctx);
	/**
	 * Visit a parse tree produced by the {@code predicatWithoutList}
	 * labeled alternative in {@link StratifiedProgramParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicatWithoutList(StratifiedProgramParser.PredicatWithoutListContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notPredicatWithoutList}
	 * labeled alternative in {@link StratifiedProgramParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotPredicatWithoutList(StratifiedProgramParser.NotPredicatWithoutListContext ctx);
	/**
	 * Visit a parse tree produced by the {@code predicatWithList}
	 * labeled alternative in {@link StratifiedProgramParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicatWithList(StratifiedProgramParser.PredicatWithListContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notPredicatWithList}
	 * labeled alternative in {@link StratifiedProgramParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotPredicatWithList(StratifiedProgramParser.NotPredicatWithListContext ctx);
	/**
	 * Visit a parse tree produced by the {@code predIdbLine}
	 * labeled alternative in {@link StratifiedProgramParser#idbrule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredIdbLine(StratifiedProgramParser.PredIdbLineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code predEdbLine}
	 * labeled alternative in {@link StratifiedProgramParser#edbrule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredEdbLine(StratifiedProgramParser.PredEdbLineContext ctx);
}